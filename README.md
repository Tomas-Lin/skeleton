# Express docker compose example

## Start

```
docker-compose up -d
```

## Refresh

```
docker-compose up -d --build
```

## Stop

```
docker-compose down
```
