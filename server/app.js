const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const dotenv = require('dotenv');
const cors = require('cors');
const passport = require('passport');

dotenv.config();

const indexRouter = require('./routes/index');
const authRouter = require('./routes/authRouter');
const homeRouter = require('./routes/homeRouter');
const userRouter = require('./routes/userRouter');

const { jwtAuthorizationMiddleware } = require("./helpers/passportManager")

const app = express();

app.set('trust proxy', 1) // trust first proxy


app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(passport.initialize());

app.use('/', indexRouter);
app.use('/auth', authRouter);
app.use('/users', userRouter);
app.use('/home', jwtAuthorizationMiddleware, homeRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500).json({ message: err.message || 'some error message' });
});

module.exports = app;
