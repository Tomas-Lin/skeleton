const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const { userModel } = require('../models');

router.get('/', function (req, res, next) {
  res.json({ title: 'user' });
})

router.post('/', async function (req, res) {
  try {
    const { name, password, account } = req.body;
    const id = mongoose.Types.ObjectId();
    const user = new userModel({
      id: id,
      name,
      password,
      account
    });
    await user.save();
    res.json({ success: true, data: user });
  } catch (error) {
    res.status(500).json({
      success: false,
      data: {
        message: error.message
      }
    })
  }
});

module.exports = router;
